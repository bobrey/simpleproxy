﻿using System;
using System.Web;
using System.Net;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
using System.IO;
using System.Data;
using System.Security.Cryptography;

namespace SimpleProxy
{
    public class HttpProxy : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (isHttpsRequired())
            {
                // maybe add !context.Request.IsLocal && in production
                if (!context.Request.IsSecureConnection)
                {
                    string redirectUrl = context.Request.Url.ToString().Replace("http:", "https:");
                    context.Response.Redirect(redirectUrl);
                }
            }
            if (!IsAuthorized(context))
            {
                context.Response.AddHeader("WWW-Authenticate", "Basic realm=\"PCMRP\"");
                context.Response.StatusCode = ((int)HttpStatusCode.Unauthorized);
                context.Response.StatusDescription = HttpStatusCode.Unauthorized.ToString();
                context.Response.Write("You must be authenticated to use this service");
                return;
            }
            string verb = context.Request.HttpMethod.ToUpper();
            string virtPath = context.Request.Url.PathAndQuery;
            string pcmrpPath = "http://172.16.100.53/";
            switch (verb)
            {
                case "GET":
                    context.Response.Write(geturl(pcmrpPath+virtPath,"",80,"",""));
                    break;
                case "POST":
                    string postdata = "";
                    using (StreamReader sr = new StreamReader(context.Request.InputStream))
                    {
                        postdata = sr.ReadToEnd();
                    }
                    context.Response.Write(getposturl(pcmrpPath+virtPath,postdata,"",80,"",""));
                    break;
            }
        }

        private bool IsAuthorized(HttpContext context)
        {
            string authUser = GetAppSetting("proxyuser");
            string authPassword = GetAppSetting("proxypassword");
            if (String.IsNullOrEmpty(authUser) || String.IsNullOrEmpty(authPassword)) return true;
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;
            string authorisationHeader = Request.ServerVariables["HTTP_AUTHORIZATION"];
            if (string.IsNullOrEmpty(authorisationHeader))
            {
               return false;
            }
            if (authorisationHeader != null && authorisationHeader.StartsWith("Basic ", StringComparison.InvariantCultureIgnoreCase))
            {
                string authorizationParameters = Encoding.Default.GetString(Convert.FromBase64String(authorisationHeader.Substring("Basic ".Length)));
                string userName = authorizationParameters.Split(':')[0];
                string password = authorizationParameters.Split(':')[1];
                if (userName.ToLower().Trim() == authUser && password.ToLower().Trim() == authPassword) return true;
            }
            return false;
        }

        public string geturl(string url, string proxyip, int port, string proxylogin, string proxypassword)
        {
            HttpWebResponse resp;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.UserAgent = "Mozilla/5.0";
            req.AllowAutoRedirect = true;
            req.ReadWriteTimeout = 5000;
            req.CookieContainer = new CookieContainer();
            req.Referer = "";
            req.Headers.Set("Accept-Language", "en,en-us");
            StreamReader stream_in;

            //WebProxy proxy = new WebProxy(proxyip, port);
            ////if proxylogin is an empty string then don't use proxy credentials (open proxy)
            //if (proxylogin != "") proxy.Credentials = new NetworkCredential(proxylogin, proxypassword);
            //req.Proxy = proxy;

            string response = "";
            try
            {
                resp = (HttpWebResponse)req.GetResponse();
                stream_in = new StreamReader(resp.GetResponseStream());
                response = stream_in.ReadToEnd();
                stream_in.Close();
            }
            catch (Exception ex)
            {
                return "Proxy error:" + ex.Message;
            }
            return response;
        }

        public string getposturl(string url, string postdata, string proxyip, short port, string proxylogin, string proxypassword)
        {
            HttpWebResponse resp;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.UserAgent = "Mozilla/5.0";
            req.AllowAutoRedirect = true;
            req.ReadWriteTimeout = 5000;
            req.CookieContainer = new CookieContainer();
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = postdata.Length;
            req.Referer = "";

            //WebProxy proxy = new WebProxy(proxyip, port);
            ////if proxylogin is an empty string then don't use proxy credentials (open proxy)
            //if (proxylogin != "") proxy.Credentials = new NetworkCredential(proxylogin, proxypassword);
            //req.Proxy = proxy;

            StreamWriter stream_out = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            stream_out.Write(postdata);
            stream_out.Close();
            string response = "";

            try
            {
                resp = (HttpWebResponse)req.GetResponse();
                Stream resStream = resp.GetResponseStream();
                StreamReader stream_in = new StreamReader(req.GetResponse().GetResponseStream());
                response = stream_in.ReadToEnd();
                stream_in.Close();
            }
            catch (Exception ex)
            {
                return "Proxy error:" + ex.Message;
            }
            return response;
        }

        private string GetAppSetting(string settingName)
        {
            System.Configuration.Configuration rootWebConfig1 =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(HttpRuntime.AppDomainAppVirtualPath);
            if (rootWebConfig1 == null) return null;
            var tempVal = rootWebConfig1.AppSettings.Settings[settingName];
            if (tempVal == null) return null;
            return tempVal.Value.Trim();
        }

        private bool isHttpsRequired()
        {
            string setting = GetAppSetting("forceSecure");
            if (setting != null && setting.ToUpper() == "TRUE") return true;
            return false;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
